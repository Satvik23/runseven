/*
 * pgBucket utilities declaration
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * Copyright (c) 2016 Dinesh Kumar
 *
 * LOCATION
 *		pgBucket/include/utils/utils.h
 */

#ifndef INCLUDE_UTILS_UTILS_H_
#define INCLUDE_UTILS_UTILS_H_

#include "../logger.h"

#include <sstream>
#include <time.h>
#include <string>
#include <iomanip>
#include "libpq-fe.h"
#include <exception>
#include <regex>
#include <atomic>
#include <csignal>
#include <thread>
#include <mutex>
#include <fcntl.h>

#define LOG(ss, llevel)	 logWriter.writeLogContent(std::stringstream(ss), (logLevels)llevel)
constexpr auto DAYSECS = 86400;
constexpr auto PGBUCKET_VERSION = "2.0-dev";
constexpr auto NEWLINE = "\n";
constexpr auto NORANGE = -1;

typedef enum timeelemnt {YEAR, MONTH, DAY, HOUR, MINUTE, SECOND} timeelement;
typedef enum runmode {FOREGROUND, BACKGROUND, INVALID_RUNMODE} runmode;

static atomic<std::time_t> myClock;
static atomic<std::time_t> dayBeginEpoch;
extern string pidDir;

class log
{
protected:
    logLevels level;
    string msg;
public:
    log(logLevels l, string str) {
        level = l;
        msg = str;
    };
    void logIt() const {
        LOG(msg, level);
    }
};

// Customized exception class.
//
class schExcp: public std::exception, protected log
{
public:
    schExcp(logLevels l, string str):log(l, str) {};
    virtual const char* what() const throw() {
        logIt();
        return msg.c_str();
    }
};

void print_usage();
void print_version();
bool checkEnv(const char* envVar, string& val);
string& ltrim(string &s, char ch=' ');
string& rtrim(string &s, char ch=' ');
string& trim(string &s, char ch=' ');
size_t getTimeElement(timeelement ele);
string& toQuote(string &s);
string parse_log_levels(logLevels);
void printMsg(string s, bool logit=false);
time_t get2DaysBeginEpoch();
string printTable(string tableHeader, vector<vector<string>>& recs, vector<string>& fields, bool isExtended=false, uint numcols=2);
// Do we need the parseIntCsv(), as a vector<template>. ??
//
// lineNum = -1 ??
// Yes, if lineNum = -1, it means, the below function is not working on
// any configuration files, and it is working as an utility, to parse
// a set of CSV integer values.
//
vector<int> parseIntCsv(string val, int from, int to, size_t lineNum=-1);

void startMyClock();
void startMyClockTicks();
std::time_t getMyClockEpoch();
void raiseRefreshSignal(pthread_t threadId);
void set2DayBeginEpoch();
int isPgBucketRunning();
bool string2Int(const string& input, int& res, int minVal=0);
bool string2Long(string& input, long &res);
int getPgBucketPid();
bool processSockResponse(string& msg);
int killPid(int pid, int signal);
int lockFile(int fd, int mode);
int unlockFile(int fd);
int getLckFileStatus(int fd, struct flock lckFile);
void validateConfigParam(string param, string value);
bool disableSignals();
bool enableSignals();
void PQClearNullptr(PGresult* res);

#endif /* INCLUDE_UTILS_UTILS_H_ */
