/*
 * types.h
 *
 *  Created on: Feb 3, 2017
 *      Author: dinesh
 */

#ifndef INCLUDE_ALIAS_H_
#define INCLUDE_ALIAS_H_

#include <string>
#include <mutex>
#include <fstream>
#include <sstream>
#include <thread>
#include <exception>
#include <regex>
#include <atomic>
#include <vector>

using stringstream = std::stringstream;
using ifstream = std::ifstream;
using ofstream = std::ofstream;
using ostream = std::ostream;
using string = std::string;
using mutex = std::mutex;
using thread = std::thread;
using exception = std::exception;
using regex = std::regex;
using smatch = std::smatch;
using LckGaurd = std::lock_guard<mutex>;
template<typename T>
using atomic = std::atomic<T>;
template<typename T>
using vector = std::vector<T>;
#endif /* INCLUDE_ALIAS_H_ */
