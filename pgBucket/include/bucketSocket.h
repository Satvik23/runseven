/*
 * pgBucket daemon's socket declaration
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/include/bucketSocket.h
 */

#ifndef INCLUDE_BUCKETSOCKET_H_
#define INCLUDE_BUCKETSOCKET_H_

#include "bucketDaemon.h"

#include <string.h>
#include <string>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <sys/un.h>

typedef struct sockaddr sockaddr;
typedef struct sockaddr_in sockaddr_in;
typedef struct sockaddr_in6 sockaddr_in6;
typedef struct sockaddr_storage sockaddr_storage;
typedef struct sockaddr_un sockaddr_un;

constexpr auto MAX_RECV_BYTES = 10240;
constexpr auto MAX_LISTENS = 10;

class bucketDaemon;

class socketServer
{
    private:
        int sFd, cFd;
        socklen_t sockLen;
        sockaddr_in sockAddr;
        sockaddr_storage ClientDetails;
        sockaddr_un usaddr;
        string sockFile;

    public:
        socketServer (string sockDir);
        void initServerSock();
        void listenAndProcessMsg (bucketDaemon *daemon);
        void sendMessage (string &msg);
};

class socketClient
{
    private:
        struct sockaddr_un localSock;
        int cFd;

    public:
        socketClient (string sockDir);
        void initClientSock();
        void sendMsgToSocket (string msg);
        string recvMsgFromSocket();
        ~socketClient();
};

#endif /* INCLUDE_BUCKETSOCKET_H_ */
