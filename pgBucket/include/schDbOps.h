﻿/*
 * pgBucket scheduler declaration
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/include/schDbOps.h
 */

#ifndef INCLUDE_SCHDBOPS_
#define INCLUDE_SCHDBOPS_

#include "dbpref.h"
#include "jobQueue.h"
#include "dbPool.h"

class schDbOps
{
    private:
        static connPooler *dbConPool;
        hashTable *jHash;
        hashTable *eHash;
        static string catalog;
        vector<string> *jobIds;
    public:
        schDbOps (connPooler *conPool, hashTable *jHash = nullptr, hashTable *eHash = nullptr);
        bool delJob (string &jId);
        bool insUpdJob (string &jobid, string &jobname, string &jobstatus,
                        string &jobType, string &jobRunFreq, string &days, string &months,
                        string &hours, string &minutes, string &seconds,
                        string &cmd, string &dbConn, string &jobEvntLoop, string &isParseCmd, string &isStoreDbCols,
                        string &jobclass, string &jobpejids, string &jobfejids, string &jobFailIfOut, bool isUpdate = false);
        bool cleanSyncHashDbTable();
        bool markStaleRunStatusAsFail();
        bool isJobIdExists (string jobId);
        // Print nJob's Jobid, Jobname, status, source file loc, error msg
        //
        void printStatJobs (char &status, string &nJobs, string &jobId, bool isExtended, uint numcols);
        hashTable *pushJobs2Hash (size_t jobId = 0);
        hashTable *pushEvnts2Hash (size_t jobId = 0);
        void syncHashWithDb (vector<vector<string>> &, std::unordered_map<string, size_t> &);
        hashTable *getJobHash();
        hashTable *getEventHash();
        void parseCronExprAndAdjust (vector<int> *, string &, const string &, string &);
        void initCatalog();
        void dropCatalog();
        void loadJobIds();
        jobClass parseJobClass (string &jclass);
        size_t *parseEventJids (string &jEvntIds);
        void printRunIdStats (string &runId, bool isExtended, uint numcols);
};


#endif
