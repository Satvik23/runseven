/*
 * pgBucket daemon's job queue declaration
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/include/jobQueue.h
 */

#ifndef INCLUDE_JOBQUEUE_H_
#define INCLUDE_JOBQUEUE_H_

#include "jobs.h"
#include <unordered_map>

const size_t _1HOUR = 3600;

// LTE stands for LessThan OR Equal To
// GT stands for GreaterThan
//
enum jobSchBuckets {
    LTE_1H = _1HOUR, LTE_2H = 2 * _1HOUR, LTE_3H = 3 * _1HOUR, GT_3H = (3 * _1HOUR + 1)
};

struct jobIdPosPair {
    size_t jobId;
    unsigned int hashPos;
};

class hashTable
{
    private:
        std::unordered_map<unsigned int, jobs *> *hTable;
        mutex hashLocker;
        int hashCounter;
        jobClass jClass;
        bool isResyncInProgress;
    public:
        hashTable (jobClass ctype);
        bool insertJob (jobs *);
        bool deleteJob (size_t);
        jobs *getJob (size_t);
        std::unordered_map<unsigned int, jobs *> *getHashTable();
        int incHashCounter();
        int getHashCounter();
        mutex &getHashLocker();
        void cleanHashTable();
        int getJobIdPos (size_t jobId);
        jobClass getJobClass() const;
        bool getIsReSyncInProgress();
        void setIsResyncInProgress (bool val);
        virtual int getJobHashTOH();
        virtual void setJobHashTOH (int toh);
        virtual string printHashTable (bool isExtended, uint numcols);
        virtual ~hashTable();
};

class jobsHashTable : public hashTable
{
    private:
        int jobHashTOH;				// Time over head while populating the hash
    public:
        jobsHashTable (jobClass ctype);
        int getJobHashTOH();
        void setJobHashTOH (int toh);
        string printHashTable (bool isExtended, uint numcols);
        ~jobsHashTable();
};

class evntsHashTable : public hashTable
{

    public:
        evntsHashTable (jobClass ctype);
        string printHashTable (bool isExtended, uint numcols);
        ~evntsHashTable();
};

class jobsQueue
{

    private:
        jobIdPosPair jobIdPos;
        size_t *nTimesRun;
        mutex jobLocker;
        static mutex jobQLocker;
        jobsQueue *next, *prev;
        static jobsQueue *front, *rear, *_1hBucket, *_2hBucket, *_3hBucket, *_gt3hBucket;
        static int jobQueueTOH;
        jobSchBuckets schBucket;
        long startInSec;
        static std::atomic<size_t> _1hJobCnt, _2hJobCnt, _3hJobCnt, _gt3hJobCnt;
        static bool isRefreshStarted;

    public:
        jobsQueue();
        jobsQueue (std::unordered_map<unsigned int, jobs *> *jobsHash);
        void setJobsBucket (jobsQueue &tmpNode, time_t nextRun, time_t noSecDiff);
        string printJobQueue (bool isExtended, uint numcols);
        size_t getJobId();

        long setJobNextRun (jobs *, time_t time);
        long getJobsNextRun();
        static size_t getSchHoursCount (jobSchBuckets schHours);
        void sortFullQueue (jobsQueue *low, jobsQueue *high);
        void doUpdBuckPntrs();
        vector<jobIdPosPair> *getJids2Dispatch (time_t, long);

        void insJob2Queue (jobIdPosPair jobId, time_t nextRun, long startInSec);
        void delJobFrmQueue (size_t jobId);
        jobsQueue *getFront();
        jobsQueue **getBucketStart (jobSchBuckets);
        jobSchBuckets getJobBucket();
        static int getJobQueueTOH();
        static void setJobQueueTOH (int toh);
        static void setIsRefreshStarted (bool val);
        static bool getIsRefreshStarted();

        static void cleanJobQ();
        jobsQueue *getNext();

        void setJobSchBucket (jobSchBuckets val);
        jobSchBuckets getJobSchBucket();
        void doUpdBuckCounters();
        // This assignment is only designed to
        // assign jobId, startInSec, schStatus, schBucket.
        //
        jobsQueue &operator= (const jobsQueue &);
};

#endif /* INCLUDE_JOBQUEUE_H_ */
