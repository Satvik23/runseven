/*
 * jobs.cpp
 *
 *      This source file has the implementation of job preferences.
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/source/jobQueue.cpp
 */
#include "../include/jobs.h"
#include "../include/dbpref.h"

schEntityPref::schEntityPref()
{
    jobStartTime = 0;
    jobEndTime = 0;
    jobPrevRunStatus = false;
    nTimesRun = 0;
    schStatus = INITIALIZED;
    jobPid = 0;
    jobStatus = false;
    jobID = 0;
    jClass = UNKNOWN_CLASS;
    jobType = UNKNOWN_TYPE;
    jobName = "";
    isSuccess = false;
    peJobIds = nullptr;
    feJobIds = nullptr;
    disableFailCnt = 0;
    faileCnt = 0;
    jobFailIfOutput = "NONE";
    isDeleted = false;
    jobCmd = "";
    jobDbConnStr = "";
    isParseCmdParams = false;
    isRecordDbResultColNames = false;
}

schEntityPref::schEntityPref (size_t id, string &name, bool jobstatus, jobTypes jobtype, jobClass jclass, string &jobcmd, string &conStr, string &jobFailIfOut,
                              size_t *pjids, size_t *fjids, int disablecnt, bool isStoreDBCols, bool isParseCmd)
{
    LOG ("Loading schedule entity preferences for the Job id -> " + std::to_string (id), DDEBUG);
    jobID = id;
    jobName = name;
    jobStatus = jobstatus;
    jobType = jobtype;
    jClass = jclass;
    jobCmd = jobcmd;
    jobDbConnStr = conStr;
    peJobIds = pjids;
    feJobIds = fjids;
    jobFailIfOutput = jobFailIfOut;
    disableFailCnt = disablecnt;
    isParseCmdParams = isParseCmd;
    isRecordDbResultColNames = isStoreDBCols;
    jobEndTime = 0;
    jobStartTime = 0;
    jobPrevRunStatus = false;
    nTimesRun = 0;
    schStatus = INITIALIZED;
    jobPid = 0;
    faileCnt = 0;
    isSuccess = false;
    isDeleted = false;
}
string schEntityPref::getJobResult() const
{
    return result;
}

void schEntityPref::setJobResult (const string &res)
{
    result = res;
}

string schEntityPref::getJobError() const
{
    return error;
}

void schEntityPref::setJobError (const string &err)
{
    error = err;
}

void jobPref::setExecPath (const string &ePath)
{
    execPath = ePath;
}

const string jobPref::getExecPath()
{
    return execPath;
}

int schEntityPref::getJobFailCnt() const
{
    return faileCnt;
}

void schEntityPref::incJobFailCounter()
{
    faileCnt++;
}

void schEntityPref::resetJobFailCounter()
{
    faileCnt = 0;
}

/*
 * jobPref:
 *      Default constructor of jobPref object.
 */
jobPref::jobPref()
{
    jobNextRun = 0;
    hrsPntPos = mnsPntPos = secPntPos = 0;
    skipNextRun = false;
    hrs = mns = secs = nullptr;
}
/*
 * jobPref:
 *      Parameterized constructor of jobPref object.
 */
jobPref::jobPref (size_t id, string &name, bool jobstatus, vector<int> *hs, vector<int> *ms, vector<int> *ss,
                  jobTypes jobtype, string &jobcmd, string &conStr, jobClass jClass, size_t *passjids, size_t *failjids,
                  string &jobFailIfOut, int disablecnt, bool isStoreDBCols, bool isParseCmd) : schEntityPref (id,  name,  jobstatus, jobtype,  jClass, jobcmd,  conStr,  jobFailIfOut,  passjids,  failjids,   disablecnt, isStoreDBCols, isParseCmd)
{
    hrs = hs;
    mns = ms;
    secs = ss;
    jobNextRun = 0;
    hrsPntPos = mnsPntPos = secPntPos = 0;
    skipNextRun = false;
}
/*
 * setJobName:
 *      Setter method of the jobName member variable
 */
void schEntityPref::setJobName (string name)
{
    jobName = name;
}
/*
 * setJobStatus:
 *      Setter method of the jobStatus member variable
 */
void schEntityPref::setJobStatus (bool status)
{
    jobStatus = status;
}
/*
 * setJobType:
 *      Setter method of the jobType member variable
 */
void schEntityPref::setJobType (jobTypes type)
{
    jobType = type;
}
/*
 * setJobHrsPntPos:
 *      Setter method of the hrsPntPos member variable
 */
void jobPref::setJobHrsPntPos (int p)
{
    hrsPntPos = p;
}
/*
 * setJobMnsPntPos:
 *      Setter method of the mnsPntPos member variable
 */
void jobPref::setJobMnsPntPos (int p)
{
    mnsPntPos = p;
}
/*
 * setJobSecPntPos:
 *      Setter method of the secPntPos member variable
 */
void jobPref::setJobSecPntPos (int p)
{
    secPntPos = p;
}
/*
 * setJobSchStatus:
 *      Setter method of the schStatus member variable
 */
void schEntityPref::setJobSchStatus (jobSchStatus status)
{
    schStatus = status;
}
/*
 * getJobSchStatus:
 *      Getter method of the schStatus member variable
 */
jobSchStatus schEntityPref::getJobSchStatus()
{
    return schStatus;
}
/*
 * getJobHrsPntPos:
 *      Getter method of the hrsPntPos member variable
 */
int jobPref::getJobHrsPntPos()
{
    return hrsPntPos;
}
/*
 * getJobMnsPntPos:
 *      Getter method of mnsPntPos member variable
 */
int jobPref::getJobMnsPntPos()
{
    return mnsPntPos;
}
/*
 * getJobSecPntPos:
 *      Getter method of secPntPos member variable
 */
int jobPref::getJobSecPntPos()
{
    return secPntPos;
}
/*
 * setJobPid:
 *      Setter method of jobPid member variable
 */
void schEntityPref::setJobPid (pid_t pid)
{
    jobPid = pid;
}
/*
 * getJobPid:
 *      Getter method of jobPid member variable
 */
pid_t schEntityPref::getJobPid()
{
    return jobPid;
}
/*
 * getJob id ->
 *      Getter method of jobID member variable
 */
size_t schEntityPref::getJobID()
{
    return jobID;
}
/*
 * getJobName:
 *      Getter method of jobName member variable
 */
string schEntityPref::getJobName()
{
    return jobName;
}
/*
 * getJobStatus:
 *      Getter method of jobStatus member variable
 */
bool schEntityPref::getJobStatus()
{
    return jobStatus;
}
/*
 * setPrevRunStatus:
 *      Setter method of jobPrevRunStatus member variable
 */
void schEntityPref::setPrevRunStatus (bool val)
{
    jobPrevRunStatus = val;
}
/*
 * getPrevRunStatus:
 *      Getter method of jobPrevRunStatus member variable
 */
bool schEntityPref::getPrevRunStatus()
{
    return jobPrevRunStatus;
}
/*
 * getJobStartTime:
 *      Getter method of jobStartTime member variable
 */
time_t schEntityPref::getJobStartTime()
{
    return jobStartTime;
}
/*
 * getJobEndTime:
 *      Getter method of jobEndTime member variable
 */
time_t schEntityPref::getJobEndTime()
{
    return jobEndTime;
}
/*
 * getJobNextRun:
 *      Getter method of jobNextRun member variable
 */
time_t jobPref::getJobNextRun()
{
    return jobNextRun;
}
/* getJobRunCounter:
 *      Getter method of nTimesRun member variable
 *
 */
size_t schEntityPref::getJobRunCounter()
{
    return nTimesRun;
}
/*
 * getJobType:
 *      Getter method of jobType member variable
 */
jobTypes schEntityPref::getJobType()
{
    return jobType;
}
/*
 * incJobRunCounter:
 *      Job run time counter
 */
void schEntityPref::incJobRunCounter()
{
    nTimesRun++;
}
/*
 * getJobHrs:
 *      Getter method of the hrs member variable
 */
vector<int> *jobPref::getJobHrs()
{
    return hrs;
}
/*
 * getJobMns:
 *      Getter method of the mns member variable
 */
vector<int> *jobPref::getJobMns()
{
    return mns;
}
/*
 * getJobSecs:
 *      Getter method of the secs member variable
 */
vector<int> *jobPref::getJobSecs()
{
    return secs;
}
/*
 * setJobHrs:
 *      Setter method of hrs member variable
 */
void jobPref::setJobHrs (vector<int> *h)
{
    if (hrs != nullptr) {
        hrs->clear();
        *hrs = *h;

    } else
        hrs = h;
}
/*
 * setJobMns:
 *      Setter method of mns member variable
 */
void jobPref::setJobMns (vector<int> *m)
{
    if (mns != nullptr) {
        mns->clear();
        *mns = *m;

    } else
        mns = m;
}
/*
 * setJobSecs:
 *      Setter method of secs member variable
 */
void jobPref::setJobSecs (vector<int> *s)
{
    if (secs != nullptr) {
        secs->clear();
        *secs = *s;

    } else
        secs = s;
}
/*
 * getNTimesRun:
 *      Getter method of nTimesRun member variable
 */
size_t *schEntityPref::getNTimesRun()
{
    return &nTimesRun;
}
/*
 * lockJob:
 *      Locks the job object
 */
void schEntityPref::lockJob()
{
    jobLock.lock();
}
/*
 * unlockJob:
 *      Unlock the job object
 */
void schEntityPref::unlockJob()
{
    jobLock.unlock();
}
/*
 * tryLock:
 *      This method tries to lock the job using RAII{Resource Acquisition Is Initialization} approach
 */
void schEntityPref::tryLock()
{
    LckGaurd lock (jobLock);
}
/*
 * setJobEndTime:
 *      Setter method of jobEndTime member variable
 */
void schEntityPref::setJobEndTime (time_t val)
{
    jobEndTime = val;
}
/*
 * setJobStartTime:
 *      Setter method of jobStartTime member variable
 */
void schEntityPref::setJobStartTime (time_t val)
{
    jobStartTime = val;
}
/*
 * setJobNextRun:
 *      Setter method of the jobNextRun member variable
 */
void jobPref::setJobNextRun (time_t val)
{
    jobNextRun = val;
}
/*
 * setJobSkipNextRun:
 *      Setter method of the skipNextRun member variable
 */
void jobPref::setJobSkipNextRun (bool val)
{
    skipNextRun = val;
}
/*
 * getJobSkipNextRun:
 *      Getter method of the skipNextRun member variable
 */
bool jobPref::getJobSkipNextRun()
{
    return skipNextRun;
}

bool schEntityPref::getJobSuccess() const
{
    return isSuccess;
}

void schEntityPref::setJobSuccess (const bool status)
{
    isSuccess = status;
}

string schEntityPref::getJobFailIfOutput() const
{
    return jobFailIfOutput;
}

void schEntityPref::setJobFailIfOutput (const string &jobFailIfOut)
{
    jobFailIfOutput = jobFailIfOut;
}

size_t *schEntityPref::getJobFailEvtIds() const
{
    return feJobIds;
}

void schEntityPref::setJobFailEvtIds (size_t *fjids)
{
    feJobIds = fjids;
}

void schEntityPref::setJobPassEvtIds (size_t *pjids)
{
    peJobIds = pjids;
}

size_t *schEntityPref::getJobPassEvtIds() const
{
    return peJobIds;
}

void schEntityPref::setDisableFailCnt (int dcnt)
{
    disableFailCnt = dcnt;
}

int schEntityPref::getDisableFailCnt() const
{
    return disableFailCnt;
}

jobClass schEntityPref::getJobClass() const
{
    return jClass;
}

void schEntityPref::setJobClass (jobClass jType)
{
    jClass = jType;
}

bool schEntityPref::getIsParseCmdParams() const
{
    return isParseCmdParams;
}

void schEntityPref::setIsParseCmdParams (bool val)
{

    isParseCmdParams = val;
}

bool schEntityPref::getIsRecordDbResCols() const
{
    return isRecordDbResultColNames;
}

void schEntityPref::setIsRecordDbResCols (bool val)
{
    isRecordDbResultColNames = val;
}

string schEntityPref::getSchStatusStr() const
{
    switch (schStatus) {
        case INITIALIZED:
            return "INITIALIZED";
            break;

        case SCHEDULED:
            return "SCHEDULED";
            break;

        case RUNNING:
            return "RUNNING";
            break;

        case RUNNING_EVENTJOB:
            return "RUNNING_EVENTJOB";
            break;

        case DISPATCHED:
            return "DISPATCHED";
            break;

        case COMPLETED:
            return "COMPLETED";
            break;

        case SKIPPED:
            return "SKIPPED";
            break;

        case KILLED:
            return "KILLED";
            break;

        default:
            return "INVALID";
            break;
    }
}

schEntityPref::~schEntityPref()
{
    if (peJobIds != nullptr)
        delete[] peJobIds;


    if (feJobIds != nullptr)
        delete[] feJobIds;
}

jobPref::~jobPref()
{
    LOG ("Job id -> " + std::to_string (getJobID()) + " destructor is invoked ", DDEBUG);

    // Event jobs won't be having hrs, mns and secs populated
    //
    if (hrs != nullptr) {
        hrs->clear();
        delete hrs;
    }

    if (mns != nullptr) {
        mns->clear();
        delete mns;
    }

    if (secs != nullptr) {
        secs->clear();
        delete secs;
    }
}

jobs::jobs (size_t id, string &name, bool jobstatus, vector<int> *hs, vector<int> *ms, vector<int> *ss,
            jobTypes jobtype, string &jobcmd, string &conStr, jobClass jClass, size_t *passjids, size_t *failjids,
            string &jobFailIfOut, int disablecnt, bool isStoreDBCols, bool isParseCmd)
    : jobPref (id,  name,  jobstatus, hs,  ms,  ss, jobtype,  jobcmd,  conStr,  jClass,  passjids,  failjids, jobFailIfOut,  disablecnt, isStoreDBCols, isParseCmd)
{
    LOG ("Loading virtual job preferences for the Job id ->" + std::to_string (id), DDEBUG);
}

osJobs::osJobs()
{
    cmdResultBuf = new char[OS_RESULT_BUFSIZE];
    fd = new int[2];
    memset (cmdResultBuf, 0, OS_RESULT_BUFSIZE);
    memset (fd, 0, 2);
    oRKind = FORK_EXEC;
}

osJobs::osJobs (size_t id, std::string &name, bool jobstatus, vector<int> *hs, vector<int> *ms, vector<int> *ss,
                jobTypes jobtype, string &cmd, osRunKind rKind, string &conStr, jobClass jClass, size_t *passjids, size_t *failjids, string &jobFailIfOut, int disablecnt, bool isStoreDBCols, bool isParseCmd)
    : jobs (id, name, jobstatus, hs, ms, ss, jobtype, cmd, conStr, jClass, passjids, failjids, jobFailIfOut, disablecnt, isStoreDBCols, isParseCmd)
{
    LOG ("Loading OS Job Preferences for the Job id ->" + std::to_string (id), DDEBUG);
    cmdResultBuf = new char[OS_RESULT_BUFSIZE];
    fd = new int[2];
    memset (cmdResultBuf, 0, OS_RESULT_BUFSIZE);
    memset (fd, 0, 2);
    oRKind = rKind;
}

osJobs::~osJobs()
{
    if (cmdResultBuf != nullptr) {
        delete[] cmdResultBuf;
        cmdResultBuf = nullptr;
    }

    if (fd != nullptr) {
        delete[] fd;
        fd = nullptr;
    }
}

dbJobs::dbJobs (size_t id, std::string &name, bool jobstatus, vector<int> *hs, vector<int> *ms, vector<int> *ss,
                jobTypes jobtype, string &cmd, string &conStr, jobClass jClass, size_t *passjids, size_t *failjids, string &jobFailIfOut, int disablecnt, bool isStoreDBCols, bool isParseCmd)
    : jobs (id, name, jobstatus, hs, ms, ss, jobtype, cmd, conStr, jClass, passjids, failjids, jobFailIfOut, disablecnt, isStoreDBCols, isParseCmd)
{
    ;
}

/*
 * isJobDeleted:
 *      Getter method for the isDeleted member variable
 */
bool schEntityPref::isJobDeleted()
{
    return isDeleted;
}
/*
 * setJobDelete:
 *      Setter method for the isDelete member variable
 */
void schEntityPref::setJobDelete (bool val)
{
    isDeleted = val;
}
/*
 * getJobCmd:
 *      Getter method for the jobCmd member variable
 */
string schEntityPref::getJobCmd()
{
    return jobCmd;
}

/*
 * killJob:
 *      This method will kill the OS jobs with either SIGKILL/SIGTERM
 */
void osJobs::killJob (bool isForce)
{
    LOG ("Trying to kill pid " + std::to_string (getJobPid()), NOTICE);

    if (kill (getJobPid(), 0) == 0) {
        // Initiating a terminate signal to the jobid
        //
        if (!isForce) {
            LOG ("Got the request to terminate the Job id ->" + std::to_string (getJobID()) + " with SIGTERM", NOTICE);
            kill (getJobPid(), SIGTERM);

        } else {
            LOG ("Got the request to terminate the Job id ->" + std::to_string (getJobID()) + " with SIGKILL", NOTICE);
            kill (getJobPid(), SIGKILL);
        }

        setJobSchStatus (KILLED);

    } else
        throw schExcp (ERROR, "Invalid pid(" + std::to_string (getJobPid()) + ") found for the jobid->" + std::to_string (getJobID()) + " to kill");
}
/*
 * runJob:
 *      This method will call execCmd function reads the output from read FD.
 */
bool osJobs::runJob (string &result, string &err, pid_t &pid)
{
    size_t jobId = getJobID();
    int eno;
    pid_t childPid = 0;
    disableSignals();

    // Assume the job execution is going to be succeeded
    //
    setJobSuccess (true);

    // Run method fork + exec
    //
    if (getOSRunKind() == FORK_EXEC) {

        LOG ("Running OS job with FORK_EXEC approach", DDEBUG);
        FILE *fp = nullptr;

        try {

            if (!execCmd (getJobCmd(), eno, childPid))
                throw schExcp (ERROR, strerror (eno));

            pid = childPid;
            setJobPid (childPid);
            // Getting results from child process
            //
            result = "";
            ssize_t readInput;
            fp = fdopen (fd[READ], "r");

            if (fp == NULL)
                throw schExcp (ERROR, strerror (errno));

            while ( (readInput = read (fileno (fp), cmdResultBuf, sizeof (cmdResultBuf) - 1)) > 0 ) {
                result += string (cmdResultBuf);
                memset (cmdResultBuf, 0, OS_RESULT_BUFSIZE);
            }

            if (readInput == -1)
                throw schExcp (ERROR, "Failed to read input from the read pipe of the child PID -> " + std::to_string (childPid) + ".Error: " + strerror (errno));

            setJobSuccess (true);
            fclose (fp);

        } catch (exception &e) {
            e.what();

            if (fp != nullptr)
                fclose (fp);

            setJobSuccess (false);
        }

    }

    // Run method posix spawn
    //
    else if (getOSRunKind() == POSIX_SPAWN) {
        LOG ("Running OS job with POSIX_SPAWN approach", DDEBUG);
        int fdIO[2];
        int fdERR[2];
        posix_spawn_file_actions_t action;

        if (pipe (fdIO) || pipe (fdERR)) {
            err = string (strerror (errno));
            LOG ("Unable to process the pipe request. Error: " + err, ERROR);
            return false;
        }

        posix_spawn_file_actions_init (&action);
        posix_spawn_file_actions_addclose (&action, fdIO[0]);
        posix_spawn_file_actions_addclose (&action, fdERR[0]);
        posix_spawn_file_actions_adddup2 (&action, fdIO[1], 1);
        posix_spawn_file_actions_adddup2 (&action, fdERR[1], 2);
        posix_spawn_file_actions_addclose (&action, fdIO[1]);
        posix_spawn_file_actions_addclose (&action, fdERR[1]);

        string cmd = getJobCmd();
        string shArgs[] = {"/bin/sh", "-c"};
        char *args[] = {&shArgs[0][0], &shArgs[1][0], &cmd[0], nullptr};

        try {
            if (posix_spawn (&childPid, args[0], &action, NULL, &args[0], NULL) != 0)
                throw schExcp (ERROR, "posix_spawn failed with error: " + string (strerror (errno)));

            pid = childPid;
            setJobPid (childPid);
            // Closing the parent's write fds
            //
            close (fdIO[1]);
            close (fdERR[1]);

            fd_set read_set;
            memset (&read_set, 0, sizeof (read_set));
            FD_SET (fdIO[0], &read_set);
            FD_SET (fdERR[0], &read_set);
            ssize_t readInput;

            while ((readInput = read (fdERR[0], cmdResultBuf, sizeof (cmdResultBuf) - 1)) > 0) {
                err += string (cmdResultBuf);
                memset (cmdResultBuf, 0, OS_RESULT_BUFSIZE);
            }

            // If we got any error message while reading the content from fd, then throw feed that message to err variable
            //
            if (readInput == -1)
                err += string (strerror (errno));

            while ((readInput = read (fdIO[0], cmdResultBuf, sizeof (cmdResultBuf) - 1)) > 0) {
                result += string (cmdResultBuf);
                memset (cmdResultBuf, 0, OS_RESULT_BUFSIZE);
            }

            if (readInput == -1)
                err += string (strerror (errno));

            // FIXME:
            //  If some of the process write some message into stderr as a print message,
            //  do we need to consider the job execution status as fail ?
            //
            //  For now, doing a double check with the process exit status,
            //  and enabling the job success irrespective of the err.empty()
            //
            if (!err.empty())
                throw schExcp (WARNING, "Got error while executing the Job id ->" + std::to_string (jobId) + ". Error: " + err);

            // Closing parent's read fds
            //
            close (fdIO[0]);
            close (fdERR[0]);
            posix_spawn_file_actions_destroy (&action);

        } catch (exception &e) {
            err = e.what();

            // Check and close fds
            //
            if (fcntl (fdIO[1], F_GETFL) != -1)
                close (fdIO[1]);

            if (fcntl (fdERR[1], F_GETFL) != -1)
                close (fdERR[1]);

            if (fcntl (fdIO[0], F_GETFL) != -1)
                close (fdIO[0]);

            if (fcntl (fdERR[0], F_GETFL) != -1)
                close (fdERR[0]);

            posix_spawn_file_actions_destroy (&action);
            setJobSuccess (false);
        }

    } else {
        LOG ("Found an invalid run method for the Job id ->" + std::to_string (jobId), ERROR);
        setJobSuccess (false);
    }

    // If childPid is set, then clear the child process resources
    //
    if (childPid != 0) {
        // Releasing resources of Child Process
        //
        int status;

        while (waitpid (childPid, &status, 0) == -1) {
            err += strerror (errno);
            LOG ("Failed to clear the resources of PID -> " + std::to_string (childPid) + ". Error: " + err, ERROR);
            setJobSuccess (false);
            break;
        }

        if (!WIFEXITED (status)) {
            err += "Job is not finished normally.";
            setJobSuccess (false);

        } else {
            LOG ("PID -> " + std::to_string (childPid) + " of the Job id ->" + std::to_string (jobId) + " is completed with exit status " + std::to_string (WEXITSTATUS (status)), DDEBUG);

            // Irrespective of the error message, check the process exit status
            // and update the job success status
            //
            if (WEXITSTATUS (status) == 0)
                setJobSuccess (true);
            else
                setJobSuccess (false);
        }

        // Check whether job got killed explicitly, and update the error message in database
        //
        if (getJobSchStatus() == KILLED ) {
            err += "Job got an external terminate or kill signal";
            setJobSuccess (false);
        }
    }

    return getJobSuccess();
}
/*
 * getOSRunKind:
 * 		Get method for the oRKind member
 */
osRunKind osJobs::getOSRunKind() const
{
    return oRKind;
}

/*
 * execCmd:
 *      This method actually runs the given os command by forking a child process and write output to write FD.
 */
bool osJobs::execCmd (string cmd, int &eno, pid_t &childPid)
{
    // Creating read/write ends
    //
#ifdef _GNU_SOURCE
    if (pipe2 (fd, O_CLOEXEC) == -1) {
        eno = errno;
        return false;
    }

#elif __APPLE__

    if (pipe (fd) == -1) {
        eno = errno;
        return false;
    }

    if (fcntl (fd[READ], F_SETFD, FD_CLOEXEC) == -1 ||
        fcntl (fd[WRITE], F_SETFD, FD_CLOEXEC) == -1) {
        fd[READ] = fd[WRITE] = -1;
        eno = errno;
        return false;
    }

#endif

    if ( (childPid = fork()) == -1 ) {
        eno = errno;
        return false;
    }

    // Parent process
    //
    if (childPid != 0 ) {
        LOG ("Created child process. PID -> " + std::to_string (childPid), DDEBUG);
        close (fd[WRITE]);
    }

    // Child process
    //
    else {
        close (fd[READ]);
        dup2 (fd[WRITE], 1);
        execl ("/bin/sh", "/bin/sh", "-c", cmd.c_str(), NULL);
        exit (3);
    }

    return true; // Parent process
}
/*
 * getDbConnStr:
 *      Getter method of dbConnStr member variable
 */
string schEntityPref::getDbConnStr()
{
    return jobDbConnStr;
}
/*
 * setDbConnStr:
 *      Setter method of dbConnStr member variable
 */
void schEntityPref::setDbConnStr (string connstr)
{
    jobDbConnStr = connstr;
}
/*
 * runJob:
 *      This method runs the actual scheduled query in database
 */
bool dbJobs::runJob (string &result, string &err, pid_t &pid)
{
    postgres *pgJobConn = new postgres (getDbConnStr());
    string cmd = getJobCmd();
    size_t jobId = getJobID();

    try {
        if (pgJobConn->connectAndGetDBconn() == nullptr)
            throw schExcp (ERROR, "Invalid db connection for the job -> " + std::to_string (jobId));

        setJobPid (pgJobConn->getDbPid());
        result = pgJobConn->execGetCSV (cmd, err, pid, getIsRecordDbResCols());
        setJobSuccess (true);
        delete pgJobConn;
        return true;

    } catch (exception &e) {
        delete pgJobConn;
        setJobSuccess (false);
        throw;
    }
}
/*
 * setJobCmd:
 *      Setter method of jobCmd member variable
 */
void schEntityPref::setJobCmd (string cmd)
{
    jobCmd = cmd;
}

/*
 * killJob:
 *      This method tries to kill the database job using pg_terminate_backend()
 */
void dbJobs::killJob (bool isForce)
{
    postgres *pgJobConn = new postgres (getDbConnStr());
    string err;

    // We can not forcefully kill the database jobs.
    // So, ignoring "isForce" flag here.
    //
    if (isForce)
        LOG ("Database jobs(Job id ->" + std::to_string (getJobID()) + " ) cannot be killed forcefully. Hence, trying with SIGTERM signal..", NOTICE);

    string emsg;

    try {
        if (pgJobConn->connectAndGetDBconn() == nullptr)
            throw schExcp (ERROR, "Unable to get the db connection for the job -> " + std::to_string (getJobID()));

        // Check whether the database has the query running with this job pid
        //
        if (pgJobConn->execGetScalar ("SELECT 1 FROM pg_catalog.pg_stat_activity WHERE pid = " + std::to_string (getJobPid()), err) == "1") {
            LOG ("Got the request to terminate the Job id ->" + std::to_string (getJobPid()), NOTICE);
            pgJobConn->execStmt ("SELECT pg_terminate_backend(" + std::to_string (getJobPid()) + ");", emsg);
            pgJobConn->closeConnection();

        } else {
            pgJobConn->closeConnection();
            throw schExcp (ERROR, "Job id ->" + std::to_string (getJobID()) + "'s pid(" + std::to_string (getJobPid()) + ") not found in the database instance");
        }

    } catch (exception &e) {
        delete pgJobConn;
        throw;
    }
}

jobs::jobs()
{
    ;
}

jobs::~jobs()
{
    ;
}

void schEntityPref::initJobSettings()
{
    setJobStartTime (getMyClockEpoch());
    incJobRunCounter();
    setJobSchStatus (DISPATCHED);
}

void schEntityPref::passJobSettings()
{
    if (getJobSchStatus() != KILLED)
        setJobSchStatus (COMPLETED);

    setPrevRunStatus (getJobSuccess());
    setJobSuccess (true);
    setJobEndTime (getMyClockEpoch());
    resetJobFailCounter();
}
void schEntityPref::failJobSettings()
{
    if (getJobSchStatus() != KILLED)
        setJobSchStatus (COMPLETED);

    // Update the job's previous run status with current run status,
    // and then update the current run status.
    //
    setPrevRunStatus (getJobSuccess());
    setJobSuccess (false);
    setJobEndTime (getMyClockEpoch());
}
